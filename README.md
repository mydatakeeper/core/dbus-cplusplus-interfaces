DBus C++ Interfaces
===================

This repository provides a set of DBus interfaces descriptions in XML format.
Using the Makefile, one can generates adaptor and proxy binding for
dbus-c++ library.
