
IFACES=\
	org.freedesktop.DBus.ObjectManager \
	org.freedesktop.DBus.Peer \
	org.freedesktop.locale1 \
	org.freedesktop.login1.Manager \
	org.freedesktop.login1.Seat \
	org.freedesktop.login1.Session \
	org.freedesktop.login1.User \
	org.freedesktop.systemd1.Automount \
	org.freedesktop.systemd1.Device \
	org.freedesktop.systemd1.Job \
	org.freedesktop.systemd1.Manager \
	org.freedesktop.systemd1.Mount \
	org.freedesktop.systemd1.Path \
	org.freedesktop.systemd1.Scope \
	org.freedesktop.systemd1.Service \
	org.freedesktop.systemd1.Slice \
	org.freedesktop.systemd1.Snapshot \
	org.freedesktop.systemd1.Socket \
	org.freedesktop.systemd1.Swap \
	org.freedesktop.systemd1.Target \
	org.freedesktop.systemd1.Timer \
	org.freedesktop.systemd1.Unit \
	org.freedesktop.timedate1

PROXY_FILES=$(foreach iface, $(IFACES), proxy/$(iface).h)
ADAPTOR_FILES=$(foreach iface, $(IFACES), adaptor/$(iface).h)

all: $(PROXY_FILES) $(ADAPTOR_FILES)


proxy:
	mkdir -p proxy

proxy/%.h: dbus-desc/%.xml proxy
	dbusxx-xml2cpp $< --proxy=$@
	sed 's|    connect_signal(\(.*\), \(.*\), \(.*\));|}\n    void connect_\2_signal() {\n    \0|g' -i $@
	sed 's| = 0;| {}|g' -i $@


adaptor:
	mkdir -p adaptor

adaptor/%.h: dbus-desc/%.xml adaptor
	dbusxx-xml2cpp $< --adaptor=$@
	sed 's| = 0;| { throw ::DBus::Error("org.freedesktop.DBus.Error.NotImplemented", "Method is not implemented"); }|g' -i $@


clean:
	rm -rf proxy adaptor
